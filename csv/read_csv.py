import csv
import sqlite3

path_cat = "category.csv"
path_scat = "sub_catagory.csv"
file_cat = open(path_cat, newline="")
file_scat = open(path_scat, newline="")
reader_cat = csv.reader(file_cat)
reader_scat = csv.reader(file_scat)

print(reader_cat)
header_cat = next(reader_cat)
header_scat = next(reader_scat)

# print(header_ca)
# print(header_fo)

data_cat = []
data_scat = []
for row in reader_cat:
    data_cat.append((row[0],row[1]))

for row in reader_scat:
        # row = [nameFood, category, priceFood, amountFood, imageUrlFood]
        # pages = int(row[7])
        # cost = float(row[8])
    data_scat.append((row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9]))

    # print(data_cat)
    # print(data_scat)
db = sqlite3.connect('../app/database.db')
cur = db.cursor()
# print(cur)
cur.execute('PRAGMA table_info(catagory)')
for var in data_cat:
    sql = 'INSERT INTO catagory(name, image) VALUES ' + str(var)
    cur.execute(sql)
    db.commit()
for var in data_scat:
    sql1 = "INSERT INTO subcatagory (name, image, type, color, size, print, corner_cut, pages, cost, c_id) VALUES " + str(var)
    print(sql1)
    cur.execute(sql1)
    db.commit()
