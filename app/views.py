from app import app
from app.model import db, user, order, measure, catagory, subcatagory
from functools import wraps
from werkzeug.security import generate_password_hash, check_password_hash
from flask import request, redirect, jsonify, make_response, session
from flask_restful import Api, Resource, reqparse, abort, fields, marshal_with
import jwt, datetime, math, re, os
from flask_cors import cross_origin

resources={
    'type': fields.String,
    'measure_length': fields.String,
    'measure_width': fields.String,
    'measure_type': fields.String,
    'color': fields.String,
    'file': fields.String,
    'user_id': fields.Integer
}
resource={
    'type': fields.String,
    'unit': fields.String,
    'unit_cost': fields.Float
}

def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token=None
        if 'access_token' in request.headers:
            token=request.headers['access_token']
        #token=request.args.get('token')
        if not token:
            return jsonify({'message': 'Token is missing'}),401
        try:
            info=jwt.decode(token, app.config['SECRET_KEY'])
            current_user=user.query.filter_by(username=info['user']).first()
        except:
            return jsonify({'message': 'Token is invalid'}),401
        return f(current_user, *args, **kwargs)
    return decorated

regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'

@app.route("/register", methods=["POST"])
@cross_origin()
def register():
    print('register')
    if request.is_json:
        print('JSON')
        req = request.get_json()
        name = req.get("name")
        email = req.get("email")
        if(re.fullmatch(regex, email)):
            pass
        else:
            return make_response(jsonify({'message': 'Invalid email'}), 400)
        username = req.get("username")
        password = req.get("password")
        c_pass = req.get("c_password")
        if password != c_pass:
            return make_response(jsonify({"message": "Password doesn't match"}), 400)
        password = generate_password_hash(password)
        address = req.get("address")
        result = user.query.filter_by(email = email).first()
        if result:
            return make_response(jsonify({'message': 'This email is already taken'}), 409)
        if user.query.filter_by(username = username).first():
            return make_response(jsonify({'message': 'This username is already taken'}), 409)
        data = user(name=name, email=email, username=username, password=password, address=address)
        db.session.add(data)
        db.session.commit()
        output = []
        output.append({
            'username' : data.username,
            'email' : data.email
        })
        return make_response(jsonify({"user": output}), 200)
    else:
        print('not JSON')
        return make_response(jsonify({'message': 'Request body must be JSON'}), 400)

@app.route("/login", methods=["POST"])
@cross_origin()
def login():
    auth = request.authorization
    if not auth.username:
        return jsonify({'message': 'Enter username'}), 400
    if not auth.password:
        return jsonify({'message': 'Enter password'}), 400
    users = user.query.filter_by(username = auth.username).first()
    if not users:
        return make_response(jsonify({'message': 'Not a valid user!!'}), 401)
    else:
        if auth and check_password_hash(users.password, auth.password):
            token=jwt.encode({'user': auth.username, 'exp': datetime.datetime.utcnow()+datetime.timedelta(minutes=15)},app.config['SECRET_KEY'])
            return jsonify({'token': token.decode('UTF-8') , 'is_admin': users.is_admin,
            'is_superadmin': users.is_superadmin})
        return make_response('Couldnt verify!',401,{'WWW-Authenticate':'Basic realm="Login Required"'})
@app.route('/view_users', methods =['POST'])
@cross_origin()
@token_required
def view_users(current_user):
    users = user.query.all()
    output = []
    if not users:
        return jsonify({'message': 'No registered user'}), 400
    if current_user.is_admin == True or current_user.is_superadmin == True:
        for i in users:
            if i == current_user:
                continue
            else:
                output.append({
                    'name' : i.name,
                    'email' : i.email,
                    'address' : i.address,
                    'is_admin': i.is_admin,
                    'is_superadmin': i.is_superadmin,
                    'id': i.id
                })
        return jsonify({'users': output}), 200
    else:
        return jsonify({'message': 'Access denied'}),401

@app.route('/update_user/<user_id>', methods =['GET','POST'])
@cross_origin()
@token_required
def update_user(current_user, user_id):
    if current_user.is_admin == True or current_user.is_superadmin == True:
        if request.is_json:
            req = request.get_json()
            name = req.get("name")
            email = req.get("email")
            if(re.fullmatch(regex, email)):
                pass
            else:
                return make_response(jsonify({"message": "Invalid email"}), 400)
            username = req.get("username")
            address = req.get("address")
            is_admin = 0
            print(is_admin)
            if current_user.is_superadmin == True:
                is_admin = req.get("is_admin")
            data = user.query.filter_by(id = user_id).first()
            if data is None:
                return jsonify({'message': 'No registered user'}), 401
            data.name = name
            data.email = email
            data.username = username
            data.address = address
            data.is_admin = is_admin
            db.session.commit()
            output = []
            output.append({
                'name' : data.name,
                'username' : data.username,
                'email' : data.email,
                'address' : data.address,
                'is_admin' : data.is_admin
            })
            return jsonify({'user': output}), 200
        else:
            return jsonify({'message': 'Request body must be JSON'}), 400
    else:
        return jsonify({'message': 'Access denied'}),401

@app.route("/delete_user/<user_id>", methods=['GET'])
@token_required
@cross_origin()
def delete_user(current_user, user_id):
    if current_user.is_superadmin == True:
        result=user.query.filter_by(id = user_id).first()
        if result:
            db.session.delete(result)
            db.session.commit()
            return jsonify({'message': 'User deleted successfully'}), 200
        else:
            return jsonify({'message': 'No registered user'}), 400
    else:
        return jsonify({'message': 'Access denied'}),401

@app.route('/measurement_type', methods =['POST'])
@token_required
@cross_origin()
@marshal_with(resource)
def add_measurement(current_user):
    if current_user.is_admin == True or current_user.is_superadmin == True:
        if request.is_json:
            req = request.get_json()
            type = req.get("type")
            unit = req.get("unit")
            unit_cost = req.get("unit_cost")
            data = measure(type= type, unit= unit, unit_cost= unit_cost)
            db.session.add(data)
            db.session.commit()
            return data, 200
        else:
            return make_response(jsonify({'message': 'Request body must be JSON'}), 400)
    else:
        return jsonify({'message': 'Access denied'}), 401

@app.route('/view_measurement_types', methods =['POST'])
@cross_origin()
@token_required
def view_measurement(current_user):
    measures = measure.query.all()
    output = []
    if not measures:
        return jsonify({"message": "Measurement type doesn't exist"}), 400
    if current_user.is_admin == True or current_user.is_superadmin == True:
        for i in measures:
            output.append({
                'type': i.type,
                'unit': i.unit,
                'unit_cost': i.unit_cost,
                'id': i.id
            })
        return jsonify({'measurement_types': output}), 200
    else:
        return jsonify({'message': 'Access denied'}),401

@app.route('/update_measurement_type/<id>', methods =['POST'])
@token_required
@cross_origin()
def update_measurement(current_user, id):
    if current_user.is_admin == True or current_user.is_superadmin == True:
        if request.is_json:
            req = request.get_json()
            type = req.get("type")
            unit = req.get("unit")
            unit_cost = req.get("unit_cost")
            data = measure.query.filter_by(id = id).first()
            if data is None:
                return jsonify({"message": "Measurement type doesn't exist"}), 400
            data.type = type
            data.unit = unit
            data.unit_cost = unit_cost
            db.session.commit()
            output = []
            output.append({
                'type' : data.type,
                'unit' : data.unit,
                'unit_cost' : data.unit_cost,
            })
            return jsonify({'measurement_type': output}), 200
        else:
            return jsonify({'message': 'Request body must be JSON'}), 400
    else:
        return jsonify({'message': 'Access denied'}),401

@app.route("/delete_measurement_type/<id>", methods=['GET','POST'])
@token_required
@cross_origin()
def delete_measurement(current_user, id):
    if current_user.is_admin == True or current_user.is_superadmin == True:
        result= measure.query.filter_by(id = id).first()
        if result:
            db.session.delete(result)
            db.session.commit()
            return jsonify({'message': 'Measurement type deleted successfully'}), 200
        else:
            return jsonify({"message": "Measurement type doesn't exist"}), 400
    else:
        return jsonify({'message': 'Access denied'}),401

@app.route('/calculate', methods = ["POST"])
@cross_origin()
def calculate():
    if request.is_json:
        req = request.get_json()
        type = req.get("type")
        measure_length = req.get("measure_length")
        measure_width = req.get("measure_width")
        measure_type = req.get("measure_unit")
        color = req.get("color")
        data = measure.query.filter_by(type= type, unit= measure_type).first()
        print(data)
        len = (data.unit_cost)*float(measure_length)
        width = data.unit_cost*float(measure_width)
        print(len,width)
        cost = len+width
        return make_response(jsonify({'Cost':cost}), 200)
    else:
        return make_response(jsonify({"message": "Request body must be JSON"}), 400)

@app.route('/order', methods =['POST'])
@token_required
@cross_origin()
def place_order(current_user):
    if request.is_json:
        req = request.get_json()
        type = req.get("type")
        measure_length = req.get("measure_length")
        measure_width = req.get("measure_width")
        measure_type = req.get("measure_type")
        file = req.get("file")
        color = req.get("color")
        id = current_user.id
        if current_user.is_admin == True or current_user.is_superadmin == True:
            id = req.get("user_id")
        data = order(type= type, measure_length= measure_length, measure_width= measure_width, measure_type= measure_type, color= color, file= file, user_id= id)
        db.session.add(data)
        db.session.commit()
        output=[]
        output.append({
            'type': data.type,
            'measure_length': data.measure_length,
            'measure_width': data.measure_width,
            'measure_type': data.measure_type,
            'color': data.color,
            'user_id': data.user_id
        })
        return jsonify({'order': output}), 200
    else:
        return make_response(jsonify({'message': 'Request body must be JSON'}), 400)

@app.route('/view_orders', methods =['GET'])
@token_required
@cross_origin()
def view_order(current_user):
    orders = order.query.all()
    output = []
    if not orders:
        return jsonify({'message': 'No order placed'}), 400
    if current_user.is_admin == True or current_user.is_superadmin == True:
        for i in orders:
            output.append({
                'type': i.type,
                'measure_length': i.measure_length,
                'measure_width': i.measure_width,
                'measure_type': i.measure_type,
                'color': i.color,
                'id': i.id,
                'user_id': i.user_id
            })
    elif current_user.is_admin == False or current_user.is_superadmin == False:
        id = current_user.id
        orders = order.query.filter_by(user_id = id)
        if not orders:
            return jsonify({'message': 'No order placed yet'}), 400
        for i in orders:
            output.append({
                'type': i.type,
                'measure_length': i.measure_length,
                'measure_width': i.measure_width,
                'measure_type': i.measure_type,
                'color': i.color
            })
    else:
        return jsonify({'message': 'Access denied'}),401
    return jsonify({'orders': output}), 200

@app.route('/update_order/<order_id>', methods =['POST'])
@token_required
@cross_origin()
def update_order(current_user, order_id):
    if current_user.is_admin == True or current_user.is_superadmin == True:
        if request.is_json:
            req = request.get_json()
            type = req.get("type")
            measure_length = req.get("measure_length")
            measure_width = req.get("measure_width")
            measure_type = req.get("measure_type")
            address = req.get("address")
            color = req.get("color")
            file = req.get("file")
            data = order.query.filter_by(id = order_id).first()
            if data is None:
                return jsonify({'message': 'No order placed'}), 400
            data.type = type
            data.measure_length = measure_length
            data.measure_width = measure_width
            data.measure_type = measure_type
            data.color = color
            data.file = file
            db.session.commit()
            output = []
            output.append({
                'type' : data.type,
                'measure_length' : data.measure_length,
                'measure_width' : data.measure_width,
                'measure_type' : data.measure_type,
                'color': data.color,
                'file': data.file
            })
            return jsonify({'order': output}), 200
        else:
            return jsonify({'message': 'Request body must be JSON'}), 400
    else:
        return jsonify({'message': 'Access denied'}),401

@app.route("/delete_order/<order_id>", methods=['GET','POST'])
@token_required
@cross_origin()
def delete_order(current_user,order_id):
    if current_user.is_admin == True or current_user.is_superadmin == True:
        result = order.query.filter_by(id = order_id).first()
        if result:
            db.session.delete(result)
            db.session.commit()
            return jsonify({'message': 'Order deleted successfully'}), 200
        else:
            return jsonify({'message': 'No order placed'}), 400
    else:
        return jsonify({'message': 'Access denied'}),401

@app.route('/catagory', methods =['POST'])
@cross_origin()
def ctgry():
    catagories = catagory.query.all()
    output = []
    if not catagories:
        return jsonify({'message': 'No catagory added'}), 400
    for i in catagories:
        output.append({
            'id': i.id,
            'name': i.name,
            'image': i.image
        })
    return jsonify({'catagories': output}), 200

@app.route('/subcatagory/<id>', methods =['POST'])
@cross_origin()
def sbctgry(id):
    s_catagory = subcatagory.query.filter_by(c_id = id)
    output = []
    result = []
    if not s_catagory:
        return jsonify({'message': 'No catagory added'}), 400
    for n,i in enumerate(s_catagory):
        output.append({
            'id': i.id,
            'name': i.name,
            'image': i.image
        })
        if n == 0:
            result.append({
                'id': i.id,
                'name': i.name,
                'image': i.image
            })
        if output[n-1]['name'] != i.name:
            print(output[n-1]['name'],'===',i.name)
            result.append({
                'id': i.id,
                'name': i.name,
                'image': i.image
            })
    return jsonify({'subcatagories': result}), 200

@app.route('/calculator/<id>', methods =['POST'])
@cross_origin()
def calculator(id):
    data = subcatagory.query.filter_by(c_id = id).all()
    output = []
    if not details:
        return jsonify({'message': 'No details added'}), 400
    for i in data:
        output.append({
            'type': i.type,
            'color': i.color,
            'size': i.size,
            'print': i.print,
            'corner_cut': i.corner_cut,
            'pages': i.pages,
            'cost': i.cost,
            'name': name.name
        })
    # val = zip(output[0].keys(),output[0].values(),output[1].values())
    # print(set(zip(output[0].values(),output[1].values())))
    # print(set(tuple(val)))
    # print(set(output))
    return jsonify({'details': output}), 200

@app.route("/logout", methods = ["POST"])
@token_required
@cross_origin()
def logout(current_user):
    session.clear()
    return jsonify({"message": "Logged out"}), 200

@app.route('/')
def test():
    return make_response(jsonify({'message':'hello'}))
