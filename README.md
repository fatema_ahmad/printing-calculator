# Printing calculator

Printing calculator is a system for calculating printing cost and placing order.

# Basic structure of printing calculator using FLASK and API

## File
* Create a `__init__`.py file to pull all of the parts of our application together into a package and then tells Python to treat it as a package
* Create a views.py file to create a new view using @app.route decorator and pass it a URL
* Create a model.py file to create database schema
* Create a run.py file to run the app using app.run()

## Folder
* env folder to create the virtual environment
* app folder to store static files, templates, `__init__`.py, views.py, model.py
* static folder for css, js, img files
* templates folder for html files

## Command to push in gitlab repository
* git init
* git add .
* git commit -m "message"
* git remote add origin "url"
* git push origin master

# System Analysis

## Super Admin
* can login to the system
* can view all user and admin information
* can add, update and delete user and admin
* can view measurement types
* can add, update and delete measurement types
* can add, view and update the orders
* can delete the orders

### Flowchart
![super_admin](/uploads/b3ad47898869c1376ab2241cab830993/super_admin.png)

## Admin:
* can login to the system
* can add, view and update all user information
* can add, view, update and delete the measurement types
* can add, view, update and delete the orders

### Flowchart
![admin](/uploads/d10cfc20858f1fd419a338319989514e/admin.png)

## User
* can register
* can login to the system
* can calculate the cost
* can place order
* can view order history

### Flowchart
![user](/uploads/47c52a8278c64506a1499fae980ff484/user.png)

## Class Diagram
![class_diagram](/uploads/8d2bb94c6135556e9665903dcd229854/class_diagram.png)

## Entity Relationship Diagram
![ER-diagram](/uploads/53ed0ea6d7cca4b3475ee4346df58273/ER-diagram.png)

## Super Admin Panel
__Login:__\
API Endpoint: `http://127.0.0.1:5000/login`\
Request data: Using basic authentication form
````
    Username
    Password
````
Response:
````
If succeed:
  {'message': token} ,200
  ````

````
If failed:
  {'message': 'Not a valid user!!'}), 400
  {'message': 'Enter username'}, 400
  {'message': 'Enter password'}, 400
  'Couldnt verify!',401,{'WWW-Authenticate':'Basic realm="Login Required"'}
  ````

__Add User:__\
API Endpoint: `http://127.0.0.1:5000/register`\
Request data:
````
    {
      "name": "",
      "email": "",
      "username": "",
      "password": "",
      "address": "",
      "is_admin": ""
    }
````  
Response:
````
If succeed:
  {"username": "", "email": ""}, 200
  ````
````
If failed:
  {'message': 'Request body must be JSON'}, 400
  {'message': 'Invalid email'}, 400
  {'message': 'This email is already taken'}, 409
  {"message": 'This username is already taken'}, 409
  ````

__View Admins and Users:__\
API Endpoint: `http://127.0.0.1:5000/view_users`\
Request data:
````
    access_token
````
Response:
````
If succeed:
  {"users":[{ "name": "", "email": "", "username": "", "address": "", "is_admin": "", "id": ""}]}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Access denied'}, 401
  {'message': 'No registered user'}, 400
````

__Update Admin and User Info:__\
API Endpoint: `http://127.0.0.1:5000/update_user/id`\
Request data:
````
    {
      "name": "",
      "email": "",
      "username": "",
      "password": "",
      "address": "",
      "is_admin": ""
    }
````  
Response:
````
If succeed:
  { "name": "", "email": "", "username": "", "address": "", "is_admin": ""}, 200
````
````
If failed:
  {'message': 'Access denied'}, 401
  {'message': 'No registered user'}, 400
  {'message': 'Request body must be JSON'}, 400
  ````

__Delete Admin and User:__\
API Endpoint: `http://127.0.0.1:5000/delete_user/id`\
Request data:
````
    access_token
````
Response:
````
If succeed:
  {'message': 'User deleted successfully'}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Access denied'}, 401
  {'message': 'No registered user'}, 401
  ````

__Add Measurement Type:__\
API Endpoint: `http://127.0.0.1:5000/measurement_type`\
Request data:
````
    access_token,
    {
      "type": "",
      "unit": "",
      "unit_cost": ""
    }
````
Response:
````
If succeed:
  {"type": "", "unit": "", "unit_cost": ""}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Access denied'}, 401
  {'message': 'Request body must be JSON'}, 400
````

__View Measurement Types:__\
API Endpoint: `http://127.0.0.1:5000/view_measurement_types`\
Request data:
````
    access_token
````
Response:
````
If succeed:
  {"measurement types":[{"type": "", "unit": "", "unit_cost": "", "id": ""}]}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Access denied'}, 401
  {"message": "Measurement type doesn't exist"}, 400
````

__Update Measurement Type:__\
API Endpoint: `http://127.0.0.1:5000/update_measurement_type/id`\
Request data:
````
    access_token,
    {
      "type": "",
      "unit": "",
      "unit_cost": ""
    }
````
Response:
````
If succeed:
  {"type": "", "unit": "", "unit_cost": ""}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Request body must be JSON'}, 400
  {'message': 'Access denied'}, 401
  {"message": "Measurement type doesn't exist"}, 400
````

__Delete Measurement Type:__\
API Endpoint: `http://127.0.0.1:5000/delete_measurement_type/id`\
Request data:
````
    access_token
````
Response:
````
If succeed:
  {'message': 'Measurement type deleted successfully'}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Access denied'}, 401
  {"message": "Measurement type doesn't exist"}, 400
````

__Add Order:__\
API Endpoint: `http://127.0.0.1:5000/order`\
Request data:
````
    access_token
    {
      "type": "",
      "measure_length": "",
      "measure_width": "",
      "measure_type": "",
      "color": "",
      "user_id": ""
    }
````
Response:
````
If succeed:
  {"type": "", "measure_length": "", "measure_width": "", "measure_type": "", "color": "", "user_id": ""}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Access denied'}, 401
  {'message': 'Request body must be JSON'}, 400
````

__View Orders:__\
API Endpoint: `http://127.0.0.1:5000/view_orders`\
Request data:
````
    access_token
````
Response:
````
If succeed:
  {"orders":[{"type": "", "measure_length": "", "measure_width": "", "measure_type": "", "color": "", "user_id": "", "id": ""}]}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Access denied'}, 401
  {'message': 'No order placed'}, 400
````

__Update Order:__\
API Endpoint: `http://127.0.0.1:5000/update_order/id`\
Request data:
````
    access_token
    {
      "type": "",
      "measure_length": "",
      "measure_width": "",
      "measure_type": "",
      "color": ""
    }
````
Response:
````
If succeed:
  {"type": "", "measure_length": "", "measure_width": "", "measure_type": "", "color": "", "user_id": ""}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Access denied'}, 401
  {'message': 'No order placed'}, 400
  {'message': 'Request body must be JSON'}, 400
````

__Delete Order:__\
API Endpoint: `http://127.0.0.1:5000/delete_order/id`\
Request data:
````
    access_token
````
Response:
````
If succeed:
  {'message': 'Order deleted successfully'}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Access denied'}, 401
  {'message': 'No order placed'}, 400
````

## Admin Panel
__Login:__\
API Endpoint: `http://127.0.0.1:5000/login`\
Request data: Using basic authentication form
````
    Username
    Password
````
Response:
````
If succeed:
  {'message': token} ,200
  ````

````
If failed:
  {'message': 'Not a valid user!!'}), 400
  {'message': 'Enter username'}, 400
  {'message': 'Enter password'}, 400
  'Couldnt verify!',401,{'WWW-Authenticate':'Basic realm="Login Required"'}
  ````

__Add User:__\
API Endpoint: `http://127.0.0.1:5000/register`\
Request data:
````
    {
      "name": "",
      "email": "",
      "username": "",
      "password": "",
      "address": ""
    }
````  
Response:
````
If succeed:
  {"username": "", "email": ""}, 200
  ````
````
If failed:
  {'message': 'Request body must be JSON'}, 400
  {'message': 'Invalid email'}, 400
  {'message': 'This email is already taken'}, 409
  {"message": 'This username is already taken'}, 409
  ````

__View Users:__\
API Endpoint: `http://127.0.0.1:5000/view_users`\
Request data:
````
    access_token
````
Response:
````
If succeed:
  {"users":[{ "name": "", "email": "", "username": "", "address": "", "is_admin": "", "id": ""}]}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Access denied'}, 401
  {'message': 'No registered user'}, 400
````

__Update User Info:__\
API Endpoint: `http://127.0.0.1:5000/update_user/id`\
Request data:
````
    {
      "name": "",
      "email": "",
      "username": "",
      "password": "",
      "address": ""
    }
````  
Response:
````
If succeed:
  { "name": "", "email": "", "username": "", "address": "", "is_admin": ""}, 200
````
````
If failed:
  {'message': 'Access denied'}, 401
  {'message': 'No registered user'}, 400
  {'message': 'Request body must be JSON'}, 400
  ````

__Add Measurement Type:__\
API Endpoint: `http://127.0.0.1:5000/measurement_type`\
Request data:
````
    access_token,
    {
      "type": "",
      "unit": "",
      "unit_cost": ""
    }
````
Response:
````
If succeed:
  {"type": "", "unit": "", "unit_cost": ""}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Access denied'}, 401
  {'message': 'Request body must be JSON'}, 400
````

__View Measurement Types:__\
API Endpoint: `http://127.0.0.1:5000/view_measurement_types`\
Request data:
````
    access_token
````
Response:
````
If succeed:
  {"measurement types":[{"type": "", "unit": "", "unit_cost": "", "id": ""}]}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Access denied'}, 401
  {"message": "Measurement type doesn't exist"}, 400
````

__Update Measurement Type:__\
API Endpoint: `http://127.0.0.1:5000/update_measurement_type/id`\
Request data:
````
    access_token,
    {
      "type": "",
      "unit": "",
      "unit_cost": ""
    }
````
Response:
````
If succeed:
  {"type": "", "unit": "", "unit_cost": ""}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Request body must be JSON'}, 400
  {'message': 'Access denied'}, 401
  {"message": "Measurement type doesn't exist"}, 400
````

__Delete Measurement Type:__\
API Endpoint: `http://127.0.0.1:5000/delete_measurement_type/id`\
Request data:
````
    access_token
````
Response:
````
If succeed:
  {'message': 'Measurement type deleted successfully'}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Access denied'}, 401
  {"message": "Measurement type doesn't exist"}, 400
````

__Add Order:__\
API Endpoint: `http://127.0.0.1:5000/order`\
Request data:
````
    access_token
    {
      "type": "",
      "measure_length": "",
      "measure_width": "",
      "measure_type": "",
      "color": "",
      "user_id": ""
    }
````
Response:
````
If succeed:
  {"type": "", "measure_length": "", "measure_width": "", "measure_type": "", "color": "", "user_id": ""}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Access denied'}, 401
  {'message': 'Request body must be JSON'}, 400
````

__View Orders:__\
API Endpoint: `http://127.0.0.1:5000/view_orders`\
Request data:
````
    access_token
````
Response:
````
If succeed:
  {"orders":[{"type": "", "measure_length": "", "measure_width": "", "measure_type": "", "color": "", "user_id": "", "id": ""}]}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Access denied'}, 401
  {'message': 'No order placed'}, 400
````

__Update Order:__\
API Endpoint: `http://127.0.0.1:5000/update_order/id`\
Request data:
````
    access_token
    {
      "type": "",
      "measure_length": "",
      "measure_width": "",
      "measure_type": "",
      "color": ""
    }
````
Response:
````
If succeed:
  {"type": "", "measure_length": "", "measure_width": "", "measure_type": "", "color": "", "user_id": ""}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Access denied'}, 401
  {'message': 'No order placed'}, 400
  {'message': 'Request body must be JSON'}, 400
````

__Delete Order:__\
API Endpoint: `http://127.0.0.1:5000/delete_order/id`\
Request data:
````
    access_token
````
Response:
````
If succeed:
  {'message': 'Order deleted successfully'}, 200
````
````
If failed:
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Access denied'}, 401
  {'message': 'No order placed'}, 400
````

## User Panel
__Calculate:__\
API Endpoint: `http://127.0.0.1:5000/calculate`\
Request data:
````
    {
      "type": "",
      "measure_length": "",
      "measure_width": "",
      "measure_type": "",
      "color": ""
    }
````
Response:
````
If succeed:
  { 'Cost': cost}, 200
  ````

````
If failed:
  {'message': 'Request body must be JSON'}, 400
  ````
__Register:__\
API Endpoint: `http://127.0.0.1:5000/register` \
Request data:
````
    {
      "name": "",
      "email": "",
      "username": "",
      "password": "",
      "address": ""
    }
````  
Response:
````
If succeed:
  {"username": "", "email": ""}, 200
  ````
````
If failed:
  {'message': 'Request body must be JSON'}, 400
  {'message': 'Invalid email'}, 400
  {'message': 'This email is already taken'}, 409
  {"message": 'This username is already taken'}, 409
  ````

__Login:__\
API Endpoint: `http://127.0.0.1:5000/login`\
Request data: Using basic authentication form
````
    Username
    Password
````
Response:
````
If succeed:
  {'message': token} ,200
  ````

````
If failed:
  {'message': 'Not a valid user!!'}), 400
  {'message': 'Enter username'}, 400
  {'message': 'Enter password'}, 400
  'Couldnt verify!',401,{'WWW-Authenticate':'Basic realm="Login Required"'}
  ````

__Place order:__\
API Endpoint: `http://127.0.0.1:5000/order`\
Request data:
````
    access_token,
    {
      "type": "",
      "measure_length": "",
      "measure_width": "",
      "measure_type": "",
      "file": "",
      "color": ""
    }
````
Response:
````
If succeed:
  { "type": "", "measure_length": "", "measure_width": "", "measure_type": "", "file": "", "color": ""}, 200
````

````
If failed:
  {'message': 'Request body must be JSON'}, 400
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'Access denied'}, 401
  ````

__View order:__\
API Endpoint: `http://127.0.0.1:5000/view_orders`\
Request data:
````
    access_token
````
Response:
````
If succeed:
  {"orders":[{"type": "", "measure_length": "", "measure_width": "", "measure_type": "", "color": "" }]}, 200
````

````
If failed:
  {'message': 'Access denied'}, 401
  {'message': 'Token is missing'}), 401
  {'message': 'Token is invalid'}), 401
  {'message': 'No order placed yet'},400
  ````
